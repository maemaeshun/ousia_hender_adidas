(function(){

  $(function() {
    init();
  })

  function init() {
    adjustHeight.init();
  }

  var adjustHeight = {
    init: function() {
      var me = this;
      me.adjustHeight();
      $(window).resize(function(){
        me.adjustHeight();
      })
    },
    adjustHeight: function() {
      if (is1280over()) return;
      var heightMax = 0;
      var wrapHeight = [];
      $(".attention .attention_wrap").each(function(i){
        // console.log( $(this).height());
        wrapHeight[i] = $(this).height();
      });

      var diff = wrapHeight[0] - wrapHeight[1];

      if (diff > 0) {
        $(".attention .attention_wrap").eq(1).css("padding-bottom", diff+40+"px");
      } else if (diff < 0) {
        $(".attention .attention_wrap").eq(0).css("padding-bottom", Math.abs(diff)+40+"px");
      } else {
        $(".attention .attention_wrap").css("padding-bottom", 40+"px");
      }
    }
  }

  var isSP = function() {
    if (window.matchMedia('screen and (max-width:767px)').matches) {
      return true;
    }
    return false
  }

  var is1280over = function() {
    if (window.matchMedia('screen and (max-width:1280px)').matches) {
      return true;
    }
    return false
  }


})()