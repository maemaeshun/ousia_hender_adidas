var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var gulpif = require('gulp-if');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var minify = require('gulp-minify-css');
var watch = require('gulp-watch');
var notify = require('gulp-notify');

var config = require('./config').sass;

// live reload
gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: './htdocs',
      // index: './dev/index.html'
      index: './index.html'
    },
    files: ['./**/*.html']
  });
});

gulp.task('sass', function () {
  return gulp.src(config.src)
    .pipe(plumber({                           // エラー出たら通知
      errorHandler: notify.onError('<%= error.message %>')
    }))
    .pipe(sass())
    .pipe(concat(config.output))              // 1つのファイルに固める
    .pipe(autoprefixer(config.autoprefixer))  // vendor-prefixつける
    .pipe(gulpif(config.minify, minify()))    // 必要ならminify
    .pipe(gulp.dest(config.dest))             // 出力
    .pipe(browserSync.reload({                // ブラウザリロード
      stream: true
    }));
});

gulp.task('watch', function () {
  return watch('./htdocs/assets/scss/**/*', function () {
    return gulp.start(['sass']);
  });
});

gulp.task('default', ['sass', 'browser-sync', 'watch']);
