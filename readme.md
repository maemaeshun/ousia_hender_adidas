# 前提
- yarnがインストール済みであること。
- gulpがグローバルにインストール済みであること。

# はじめに
まず、このソースコードをpullしたら、プロジェクトの作業フォルダにコピーする。
package.jsonのnameなどを適宜編集して変更する。

# 依存ライブラリのインストール
作業フォルダで以下コマンドを実行する。
```
yarn install
```

# ディレクトリ構成
```
htdocs
├─css                  自前のcssファイル（このスケルトンではstyle.cssが生成される）
│  └─lib
│      └─vendor       cssライブラリ
├─dev                  開発用の資源置き場
├─fonts                フォント
├─images               画像
├─js                   自前のjsファイル
│  └─lib
│      └─vendor       jsライブラリ
├─scss                 scssファイル群
└─src                  ビルド前のjs（このスケルトンでは使用しない）
```

# デフォルトで導入されているライブラリ
- jQuery3.1.1
- Bootstrap3.3.7

# style.scss
- リセットcssとしてsanitize.cssを導入している。
- Bootstrapを使う場合は内部でnormalize.cssが使用されているので、上記を無効化する
- notoフォントを読み込む場合は_noto_fonts.scssのインポートを有効化し、eagletにあるサブセット化されたフォントファイルをfonts以下に配置する。

# gulpタスク
デフォルトタスクでscss・htmlを監視する。変更があればscssをビルドし直す。
また、browser-syncも自動で起動するようになっている。
```
gulp
```

# auto-prefixer
auto-prefixerを導入しているので、ベンダプレフィックスは気にしなくて良い。
auto-prefixerの設定はconfig.jsにあるので適宜変更する。

# cssの圧縮
config.jsのminifyを**true**にしてビルドする。

# 開発用ページ一覧
dev/index.htmlを適宜メンテナンスして、開発資源にアクセスしやすくしておくこと。
